var clientesObtenidos;

function getClientes() {
 var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
//Nombre, Ciudad, Bandera
 var request = new XMLHttpRequest();

 request.onreadystatechange = function() {
   console.log(this);
   if(this.readyState === 4 && this.status === 200) {
     //console.log(request.responseText);
     clientesObtenidos = request.responseText;
     procesaClientes();
   }
 };

 request.open("GET", url, true);
 request.send();
}

function procesaClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);

  var divTabla = window.document.getElementById("divTablaCliente");

  var tabla = window.document.createElement("table");

  var thead = window.document.createElement("thead");

  var head = window.document.createElement("tr");
  var headNombre = window.document.createElement("th");
  headNombre.innerText = "Nombre Contacto";
  var headCiudad = window.document.createElement("th");
  headCiudad.innerText = "Ciudad";
  var headFlag = window.document.createElement("th");
  headFlag.innerText = "Bandera";

  head.appendChild(headNombre);
  head.appendChild(headCiudad);
  head.appendChild(headFlag);

  thead.appendChild(head);

  var tbody = window.document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var x=0; x<JSONClientes.value.length; x++) {

    var nuevaFila = window.document.createElement("tr");
    var columnaNombre = window.document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[x].ContactName;
    var columnaCiudad = window.document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[x].City;
    var columnaBandera = window.document.createElement("td");

    var img = window.document.createElement("img");
    var imgSrc = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + (JSONClientes.value[x].Country == 'UK' ? 'United-Kingdom' : JSONClientes.value[x].Country  ) + ".png";

    img.setAttribute("src", imgSrc);
    img.classList.add("flag");

    columnaBandera.appendChild(img);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(thead);
  tabla.appendChild(tbody);

  divTabla.appendChild(tabla);

}
