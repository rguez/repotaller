var productosObtenidos;

function getProductos() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();

  request.onreadystatechange = function() {
    if(this.readyState === 4 && this.status === 200) {
      //console.log(request.responseText);
      productosObtenidos = request.responseText;
      procesarProductos();

    }
  };

  request.open("GET", url, true);
  request.send();
}

function procesarProductos() {
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);

  var divTabla = window.document.getElementById("divTabla");

  var tabla = window.document.createElement("table");

  var thead = window.document.createElement("thead");

  var head = window.document.createElement("tr");
  var headNombre = window.document.createElement("th");
  headNombre.innerText = "Nombre Producto";
  var headPrecio = window.document.createElement("th");
  headPrecio.innerText = "Precio";
  var headStock = window.document.createElement("th");
  headStock.innerText = "Unidaes en Inventario";

  head.appendChild(headNombre);
  head.appendChild(headPrecio);
  head.appendChild(headStock);

  thead.appendChild(head);

  var tbody = window.document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for(var x=0; x<JSONProductos.value.length; x++) {

    var nuevaFila = window.document.createElement("tr");
    var columnaNombre = window.document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[x].ProductName;
    var columnaPrecio = window.document.createElement("td");
    columnaPrecio.innerText = JSONProductos.value[x].UnitPrice;
    var columnaStock = window.document.createElement("td");
    columnaStock.innerText = JSONProductos.value[x].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

  }
  tabla.appendChild(thead);
  tabla.appendChild(tbody);

  divTabla.appendChild(tabla);
}
